<?php
class Querybuilder{

	public $pdo;

	public function __construct($pdo){
		$this->pdo = $pdo;
	}

	public function selectAll($table){
		$classname = substr(ucfirst($table),0,-1);

		$stmt = $this->pdo->prepare("SELECT * FROM $table");
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_CLASS,$classname);
	}

	public function selectwithid($table,$id){
		$classname = substr(ucfirst($table),0,-1);

		$stmt = $this->pdo->prepare("SELECT * FROM $table WHERE id=$id");
		$stmt->execute();


		//return $stmt->fetchAll(PDO::FETCH_CLASS,$classname)[0];

		$stmt->setFetchMode(PDO::FETCH_CLASS,$classname);
		return $stmt->fetch();
	}

    public function delete($table,$id){
        $stmt = $this->pdo->prepare("DELETE FROM $table WHERE id=$id");
        $stmt->execute();
    }


    public function insert($table,$parameters){
		$sql = sprintf(
			'insert into %s (%s) values (%s)',
			$table,
			implode(', ',array_keys($parameters)),
			':'.implode(', :',array_keys($parameters))	
	 	);

	 	try{
	 		$stmt = $this->pdo->prepare($sql);

	 		$stmt->execute($parameters);
	 	}
	 	catch(PDOException $e){
	 		die($e->getMessage());
	 	}


	}

	public function update($table,$parameters,$id){
		// UPDATE products SET naam = :naam, prijs = :prijs, btwtarief = :btwtarief, omschrijving = :omschrijving WHERE id = 1

		foreach(array_keys($parameters) as $key){
			$setjes[] = $key.' = :'.$key;
		}
	
		$sql = sprintf(
			'update %s set %s where id=%s',
			$table,
			implode(', ',$setjes),
			$id
		);

		try{
	 		$stmt = $this->pdo->prepare($sql);

	 		$stmt->execute($parameters);
	 	}
	 	catch(PDOException $e){
	 		die($e->getMessage());
	 	}
	}













}