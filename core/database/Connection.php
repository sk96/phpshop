<?php

class Connection{


	//STATIC FUNCTION: je moet geen object aanmaken om gebruik te maken van deze functie
	public static function make(){
		//try maak en return pdo object and catch -> probeer, indien niet gelukt -> actie
		try{
			return new PDO('mysql:host=localhost;dbname=phpshop','suzanne','suzanne');
		}
		catch (PDOException $e){
			die($e->getMessage());
		}
		
	}
}